module Typed_lambda where

data Type = TBool 
           | TInt 
           | Func Type Type 
           | Error String 
           deriving (Eq, Show)

data Constant = CInt Int 
               | CBool Bool 
               deriving (Eq, Show)

data Term = Var String
           | Abs String Type Term
           | App Term Term
           | Lit Constant
           deriving (Eq, Show)

newtype Context = Context [(String, Type)] 
                  deriving (Show)

-- Создание пустного контекста
contextInit :: Context
contextInit = Context []

-- Расширение контекста новым утверждением
extend :: String -> Type -> Context -> Context
extend x t (Context cx) = Context $ (x,t) : cx

-- Data.List lookup :: Eq a => a -> [(a, b)] -> Maybe b
-- Поиск переменной и ее типа в данном контексте
findVar :: Context -> String -> Type
findVar (Context cx) x = 
	case lookup x cx of
		Nothing -> Error $ "There is no variable named " ++ x ++ " in this context!"
		Just t -> t
		
termTypeCheck :: Context -> Term -> Type
termTypeCheck _ (Lit CInt{}) = TInt
termTypeCheck _ (Lit CBool{}) = TBool

--  x : T ∈ Γ
--  ---------- (T-Var)
--  Γ |- x : T
termTypeCheck cx (Var x) = findVar cx x

--   Γ, x : T1 |- e : T2
--  ---------------------- (T-Abs)
--  Γ |- λ x . e : T1 → T2
termTypeCheck cx (Abs x t e) = let new_cx = extend x t cx 
                                   te = termTypeCheck new_cx e 
                                in  Func t te 

--  Γ |- e1 : T11 → T12   Γ |- e2 : T11
--  ----------------------------------- (T-App)
--          Γ |- e1 e2 : T12
termTypeCheck cx (App f a) = let tf = termTypeCheck cx f
	      	                  in case tf of 
      		                        Func ltf rtf -> do
      		                                          let ta = termTypeCheck cx a
      		                                           in if ltf /= ta 
      		                        	                     then (Error "Bad function argument type")
      		                       	                         else rtf
      		                        _ -> Error "There is no function in first argument of application"


getType :: Term -> Type
getType t = termTypeCheck contextInit t 


-------------------------------------------------- Examples --------------------------------------------------
-- (λx : Int -> Int . x) (λy : Int. y) 3
t0 = App (App (Abs "x" (Func TInt TInt) (Var "x")) (Abs "y" TInt (Var "y"))) (Lit (CInt 10))
-- Wrong example: (λx : Int -> Int . x) 3
t1 = App (Abs "x" (Func TInt TInt) (Var "x")) (Lit (CInt 27))

main = do
	putStr "Type of (Lx : Int -> Int . x) (Ly : Int. y) 10 : "
	print $ getType t0
	putStr "Type of (Lx : Int -> Int . x) 27 : "
	print $ getType t1