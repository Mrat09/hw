
-- Аппликативные функторы (см. строку 61 и далее)
import Control.Applicative

newtype SomeType = SomeConstructor Int deriving (Eq,Show)

-- Классы типов
    
class SomeClass a where
    someFun1 :: a -> Int
    someFun2 :: Int -> a

class Show' a where
    show' :: a -> String

class Eq' a where
    eq :: a -> a -> Bool

-- Экземпляры классов типов
          
instance SomeClass SomeType where
    someFun1 (SomeConstructor x) = x
    someFun2 x = SomeConstructor x

-- someFun1 (SomeConstructor 5)

instance Show' SomeType where
    show' (SomeConstructor x) = "Some(" ++ show x ++ ")"

instance Eq' SomeType where
    eq (SomeConstructor x) (SomeConstructor y) = x == y

neq :: (Eq' a) => a -> a -> Bool
neq x y = not $ eq x y

showEquality :: (Eq' a, Show' a) => a -> a -> String
showEquality x y = if x `eq` y
                   then show' x ++ " == " ++ show' y
                   else show' x ++ " /= " ++ show' y

-- Функторы

class Functor' f where
    fmap' :: (a -> b) -> f a -> f b

instance Functor' [] where
    fmap' g [] = []
    fmap' g (x:xs) = g x : fmap' g xs

instance Functor' Maybe where
    fmap' g Nothing = Nothing
    fmap' g (Just x) = Just $ g x

instance Functor' (Either c) where
    fmap' g (Left c) = Left c
    fmap' g (Right a) = Right $ g a

instance Functor' ((->) c) where
    fmap' = (.)

-- Должно выполняться: fmap id === id
            
-- Аппликативные функторы
-- из Control.Applicative

class Functor' f => Applicative' f where
    pure' :: a -> f a
    (<*>>) :: f (a -> b) -> f a -> f b

-- Законы:
-- pure id <*> x  ===  x

-- pure (.) <*> u <*> v <*> w  ===  u <*> (v <*> w)
-- без контейнера: (u . v) w === u (v w)

-- pure f <*> pure x  ===  pure (f x)

-- u <*> pure y  ===  pure ($ y) <*> u
              
instance Applicative' Maybe where
    pure' x = Just x

    Nothing <*>> _ = Nothing
    _ <*>> Nothing = Nothing
    Just g <*>> Just x = Just $ g x

